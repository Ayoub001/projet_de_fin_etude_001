#!/bin/bash

cd /var/lib/jenkins/workspace/django_react_pipeline_main/endafrontend
npm install
npm audit fix
npm audit fix --force
