# Generated by Django 4.1.7 on 2023-09-06 09:24

from django.db import migrations, models
import unixtimestampfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0048_rename_simulatuion_simulatuion_index_file_and_more'),
    ]

    operations = [
        migrations.CreateModel(
            name='visualisation',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('device_id', models.CharField(default='device_id', max_length=256)),
                ('bytes_ts', models.IntegerField(default=None)),
                ('bytes_fs', models.IntegerField(default=None)),
                ('timestamp', unixtimestampfield.fields.UnixTimeStampField(default=None)),
            ],
            options={
                'verbose_name_plural': '20. Simulatuion_VR',
            },
        ),
        migrations.AlterModelOptions(
            name='simulatuion',
            options={'verbose_name_plural': '19. Simulatuion_VR'},
        ),
    ]
