import { useState } from "react";
import React from "react";
import "./App.css";
import BarChart from "./components/BarChart";
import LineChart from "./components/LineChart";
import { UserData } from "./Data";
function Visualisation() {
var today = new Date();
var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
  const [userData, setUserData] = useState({
    labels: UserData.map((data) => time),
    datasets: [
      {
        label: "Users Losted",
        data: UserData.map((data) => data.userLost),
        backgroundColor: [
          "rgba(75,192,192,1)",
          "#ecf0f1",
          "#50AF95",
          "#f3ba2f",
          "#2a71d0",
        ],
        borderColor: "black",
        borderWidth: 2,
      },
    ],
  });

  // IF YOU SEE THIS COMMENT: I HAVE GOOD EYESIGHT

  return (
    <>
    <br></br>
    <div className="row">
    <div className="col-sm-6">
      <div className="card">
        <div className="card-body">
        <div style={{ width: 400 }}>
        <h5 className="card-title">Special title treatment</h5>
        <BarChart chartData={userData} />
          </div>
        </div>
      </div>
    </div>
    <div className="col-sm-6">
      <div className="card">
        <div className="card-body">
        <div style={{ width: 400 }}>
        <h5 className="card-title">Special title treatment</h5>
        <LineChart chartData={userData} />
      </div>
        </div>
      </div>
    </div>
  </div>
  <br></br>
  </>
  );
}

export default Visualisation;
