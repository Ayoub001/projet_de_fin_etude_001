
import {Link} from 'react-router-dom'; 
import {useState} from 'react';
import React from 'react'

function Header() {
const [searchString, setsearchString]=useState({
   'search': ''
});
const teacherLoginStatus=localStorage.getItem('teacherLoginStatus'); 
const studentLoginStatus=localStorage.getItem('studentLoginStatus');
const handleChange=(event)=>{
setsearchString({
  ...searchString,
[event.target.name]: event.target.value
});

}
const searchCourse=() =>{
if(searchString.search!==''){
window.location.href='/search/'+searchString.search
}
}


let menu1;
let menu;

if (studentLoginStatus==='true') {
  menu = (

    <ul className="sub-menu">
      <li>
      <Link to="/user-dashboard">Dashboard</Link>
      </li>
      <li>
      <Link to="/user-logout">Logout</Link>
      </li>
    </ul>
        
  );
}
else {
  menu = (
    <ul className="sub-menu">
      <li>
      <Link to="/user-login">Login</Link>
      </li>
      <li>
      <Link to="/user-register">Register</Link>
      </li>
    </ul>
  );
}
if (teacherLoginStatus==='true') {
  menu1 = (

    <ul className="sub-menu">
      <li>
      <Link to="/teacher-dashboard">Dashboard</Link>
      </li>
      <li>
      <Link to="/teacher-logout">Logout</Link>
      </li>
    </ul>
        
  );
}
else {
  menu1 = (
    <ul className="sub-menu">
      <li>
      <Link to="/teacher-login">Login</Link>
      </li>
      <li>
      <Link to="/teacher-register">Register</Link>
      </li>
    </ul>
  );
}


return (
  <>
  
  <header className="navbar-area">
    <nav className="navbar navbar-expand-lg">
      <div className="container nav-container">
        <div className="responsive-mobile-menu">
          <button
            className="menu toggle-btn d-block d-lg-none"
            datatarget="#themefie_main_menu"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="icon-left"></span>
            <span className="icon-right"></span>
          </button>
        </div>
        <div className="logo">
        
        <Link to="/">
            <h2 class="text-primary"><Link to="/">EndaForLearn..</Link></h2>
        </Link>
        </div>
        <div className="collapse navbar-collapse" id="themefie_main_menu">
          <ul className="navbar-nav menu-open text-end">
            <li className="current-menu-item">
              <Link href="/">
              <form className="d-flex">
                  <input name="search" onChange={handleChange} className="form-control me-2" type="search" aria-label="Search" style={{height: "2.5em", marginTop:"1em"}} />
                  <button onClick={searchCourse} className="btn btn-warning" type="button" style={{width: "6em"}} >Search</button>
              </form>
              </Link>
            </li>
            </ul>
            </div>
        
        <div className="nav-right-part nav-right-part-mobile">
          <ul>
            <li>
              <a className="" href="#">
                <i className="fa fa-shopping-basket"></i>
              </a>
            </li>
          </ul>
        </div>
        <div className="collapse navbar-collapse" id="themefie_main_menu">
          <ul className="navbar-nav menu-open text-end">
            <li className="current-menu-item">
            <Link className="nav-link active" aria-current="page" to="/">Home</Link>
            </li>

            <li className="menu-item-has-children">
            <Link to="/all-courses">Courses</Link>
              <ul className="sub-menu" id="demo">
                
              </ul>
            </li>
            <li className="current-menu-item">
            <Link to="/category">Categories</Link>
            </li>
            <li className="current-menu-item">
            <Link to="/contact-us">Contact-Us</Link>
            </li>
            <li className="current-menu-item">
              <Link href="/about">
              <Link to="/aboutus">About</Link>
              </Link>
            </li>

            <li className="menu-item-has-children">
              <a className="">
                <i className="fa fa-users"></i>
              </a>
              {menu}
            </li>
            <li className="menu-item-has-children">
              <a className="">
              <i className="fas fa-chalkboard-teacher"></i>
              </a>
              {menu1}
            </li>
            <li>
                          
            </li>
            <li className="current-menu-item">
            <a className="nav-link nav-item" target='__blank' href="http://127.0.0.1:8000/admin/login/?next=/admin/">Admin</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    
  </header>
</>
)
}
export default Header;
