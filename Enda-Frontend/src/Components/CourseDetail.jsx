import React from 'react'
import CourseQuizList from './User/CourseQuizList';
import { useParams } from 'react-router-dom'
import { Link } from 'react-router-dom';
import { useEffect } from 'react'
import { useState } from 'react'
import Swal from 'sweetalert2'
import axios from 'axios'
import './main.css'
import './style.css'
import './bootstrap.min.css'

const baseUrl='http://127.0.0.1:8000/api'
const siteUrl='http://127.0.0.1:8000/'

const CourseDetail = () => {
  const [courseData, setcourseData]=useState([]);
  const [simulatuionData, setsimulatuionData]=useState([]);
  const [chapterData, setchapterData]=useState([]);
  const [teacherData, setteacherData]=useState([]);
  const [realtedcourseData, setrealtedcourseData]=useState([]); 
  const [techList, settechList]=useState([]);
  const [userLoginStatus, setuserLoginStatus]=useState();
  const [enrollStatus, setenrollStatus]=useState(); 
  const [ratingStatus, setratingStatus]=useState(); 
  const [courseViews, setcourseViews]=useState(0); 
  const [AvgRating, setAvgRating]=useState(0);
  const [favoriteStatus, setfavoriteStatus]=useState();
  let {course_id}=useParams();
  const studentId=localStorage.getItem('studentId'); 
  // Fetch courses when page load
  useEffect (()=>{
  // Fetch Courses
  try{
  axios.get(baseUrl+'/course/'+course_id)
  .then((res)=>{
  console.log(res);
  setcourseData(res.data);
  setsimulatuionData(res.data.course_simulatuion);
  setchapterData(res.data.course_chapters);
  setteacherData(res.data.teacher);
  setrealtedcourseData (JSON.parse(res.data.related_videos));
  settechList(res.data.tech_list);
  if(res.data.course_rating!=='' && res.data.course_rating!==null) { 
    setAvgRating (res.data.course_rating)
  }
  });
  // Update View
  axios.get(baseUrl+'/update-view/'+course_id) 
  .then((res)=>{
    setcourseViews(res.data.views)
  });
  }catch(error) {
  console.log(error);
  }
  
  // Fetch enroll status
  try{
  axios.get(baseUrl+'/fetch-enroll-status/'+studentId+'/'+course_id)
  .then((res)=>{
  if(res.data.bool===true){
    setenrollStatus('success');
  }
  });
  
  }catch(error) {
    console.log(error);
  }
  
  // Fetch rating status
  try{
  axios.get(baseUrl+'/fetch-rating-status/'+studentId+'/'+course_id)
  .then((res)=>{
  if (res.data.bool === true) {
    setratingStatus('success');
  }
  });
  }catch(error) {
  console.log(error);
  }
  
  // Fetch enroll status
  try{
  axios.get(baseUrl+'/fetch-favorite-status/'+studentId+'/'+course_id)
  .then((res)=>{
  if (res.data.bool === true){
  setfavoriteStatus('success');
  }else{
  setfavoriteStatus('');
  }
  });
  
  }catch(error) {
    console.log(error);
  }
  const studentLoginStatus=localStorage.getItem('studentLoginStatus');
  if(studentLoginStatus === 'true'){
  setuserLoginStatus('success');
  }
  },[]);
  
  // Enroll in the course 
  const enrollCourse = ()=>{
  const _formData=new FormData(); 
  _formData.append('course', course_id); 
  _formData.append('student', studentId);
  try{
  axios.post(baseUrl+'/student-enroll-course/',_formData, {
  headers: {
  'content-type': 'multipart/form-data'
  }
  })
  .then((res)=>{
  if(res.status===200||res.status===201){
  Swal.fire({
  title: 'You have successfully enrolled in this course',
  icon: 'success',
  toast: true,
  timer: 10000,
  position: 'top-right',
  timerProgressBar:true,
  showConfirmButton: false
  });
  setenrollStatus('success');
  }
  });
  }catch(error) {
  console.log(error);
  }
  }
  
  // Mark as favorite Course
  const markAsFavorite = ()=>{
  const _formData=new FormData(); 
  _formData.append('course', course_id); 
  _formData.append('student', studentId); 
  _formData.append('status', true);
  try{
  axios.post(baseUrl+'/student-add-favorte-course/', _formData,{
  headers: {
  'content-type': 'multipart/form-data'
  }
  })
  .then((res)=>{
  if(res.status===200||res.status===201){
  Swal.fire({
  title: 'This course has been added in your wish list',
  icon: 'success',
  toast: true,
  timer:10000,
  position: 'top-right',
  timerProgressBar:true, 
  showConfirmButton: false
  });
  setfavoriteStatus('success');
  }
  });
  }catch(error) {
    console.log(error);
  }
  }
  // End
  
  // Remove from favorite 
  const removeFavorite=(pk)=>{ 
    const _formData=new FormData(); 
    _formData.append('course', course_id); 
    _formData.append('student', studentId); 
    _formData.append('status', false);
  try{
  axios.get(baseUrl+'/student-remove-favorite-course/'+course_id+'/'+studentId,{
  headers: {
    'content-type': 'multipart/form-data'
  }
  })
  .then((res)=>{
  if(res.status===200||res.status===201) {
  Swal.fire({
  title: 'This course has been removed from your wish list',
  icon: 'success',
  toast:true,
  timer:10000,
  position: 'top-right',
  timerProgressBar:true,
  showConfirmButton: false
  });
  setfavoriteStatus('');
  }
  });
  }catch(error) {
    console.log(error);
  }
  }
  //End
  
  // Add Rating
  const [ratingData, setratingData]=useState({ 
    rating:'',
     reviews: ''
  });
  const handleChange=(event)=>{
  setratingData({
  ...ratingData,
  [event.target.name]: event.target.value
  });
  }
  const formSubmit=()=>{
  const _formRatingData=new FormData();
  _formRatingData.append('course', course_id); 
  _formRatingData.append('student', studentId); 
  _formRatingData.append('rating', ratingData.rating); 
  _formRatingData.append('reviews', ratingData.reviews);
  try{
  axios.post(baseUrl+'/course-rating/',_formRatingData,{
  headers: {
  'content-type': 'multipart/form-data'
  }
  })
  .then((res)=>{
  if(res.status===200 || res.status===201) { 
    Swal.fire({
  title: 'Rating has been saved', 
  icon: 'success',
  toast: true,
  timer: 5000,
  position: 'top-right',
  timerProgressBar:true, 
  showConfirmButton: false
  });
  window.location.reload();
  }
  });
  }catch(error) {
  console.log(error);
  }
  
  };
  useEffect(() => {
    window.scrollTo(0, 0)
  }, [])
  useEffect(() => {
    let videoList = document.querySelectorAll('.video-list-containernb .list');
  
    videoList.forEach(vid => {
        vid.onclick = () => {
            videoList.forEach(remove => { remove.classList.remove('active') });
            vid.classList.add('active');
            let src = vid.querySelector('.list-video').src;
            let title = vid.querySelector('.list-title').innerHTML;
            // let lestitle = vid.querySelector('.title').innerHTML;
            document.querySelector('.main-video-containernb .main-video').src = src;
            document.querySelector('.main-video-containernb .main-video').play();
            document.querySelector('.main-video-containernb .main-vid-title').innerHTML = title;
            
    
        };
    });
  })
  return (
  
  <>
  
  <section className="courses-details-area pd-top-135 pd-bottom-130">
          <div className="container">
                        <div class="containernb" >
                         <div style={{width:"60%"}}>
                        {chapterData ? chapterData.slice(0,1).map((item) => (
                            
                            <div class="main-video-containernb">
                            
                <video src={item.video} loop controls class="main-video"></video>
                <h3 class="main-vid-title">{item.title}</h3>
                </div >

                        
                      )) : <p>no post yet!</p>}
                      
                      </div> 
  
                              
                                    

                        <div class="video-list-containernb" >
                        
                        <h5 className="widget-title border-0">Lessons</h5>
                        {chapterData ? chapterData.map((item) => (
                          
                          <div>
                            <br></br>
                                  {item.id !== course_id ? (
                                  
                                  <div class="list active">
                                  
                          <video src={item.video} class="list-video"></video>
                          <h3 class="list-title">{item.title}</h3>
                          <h3 hidden class="list-desc">{item.title}</h3>
                      </div>
                          
                      )
                      : (<input hidden />)}
                                </div>
                            )) : <p>no post yet!</p>}
                            
                            <div>
                              <br></br>
                                    
                                  </div>
  
                        </div>
  
                        </div>

  
                  
      
       
            <div className="row">
              <div className="col-lg-8">
                
                <div className="single-course-wrap mb-0">
                
  
                  <div className="wrap-details">
                    <h5>
                      <a href="#">{courseData.title}</a>
                    </h5>
                    <p>{courseData.description}</p>
                    <hr></hr>
                    <div className="user-area">
                      <div className="user-details">
                        <img src="/assets/img/author/1.png" alt="img" />
                        <a href="#"> </a>
                      </div>
                      <div className="date ms-auto">
                        <i
                          className="fa fa-calendar-alt me-2"
                          style={{ color: "var(--main-color)" }}
                        ></i>
                      
                      </div>
                    </div>
                  
                  </div>
                </div>
                
                <div className="buying-wrap d-flex align-items-center pd-bottom-35 pd-top-5">
                  <h2 className="price d-inline-block mb-0"></h2>
  
                  {userLoginStatus==='success' && enrollStatus !== 'success' &&
                                  <button
                                  onClick={enrollCourse}
                                    className="btn btn-sm btn-base ms-auto mx-2 p-1">        
                                    Enroll Now
                                  </button>
                                } 
                  {enrollStatus==='success' && userLoginStatus === 'success' &&
                                    <p><span>You are arleady enrolled in this course</span></p>
                                    }
                  {userLoginStatus !== 'success' &&
                    <p><Link to="/user-login">Please login to enroll in this course</Link></p>
                    }              
                  <div className="ms-auto d-425-none">
                  {userLoginStatus === 'success' && favoriteStatus !== 'success' &&
                    <button  onClick={markAsFavorite}>
                      <i className="far fa-heart"></i>
                    </button>
                    }
                    { userLoginStatus==='success' && favoriteStatus==='success' &&
                    <button  onClick={removeFavorite}>
                      <i className="far fa-heart"></i>
                    </button>
                    }
                  </div>
                </div>
                <ul className="course-tab nav nav-pills pd-top-25">
                  <li className="nav-item">
                    <button
                      className="nav-link active"
                      id="pill-1"
                      data-bs-toggle="pill"
                      data-bs-target="#pills-01"
                      type="button"
                      role="tab"
                      aria-controls="pills-01"
                      aria-selected="true"
                    >
                      Overview
                    </button>
                  </li>
                  <li className="nav-item">
                    <button
                      className="nav-link"
                      id="pill-2"
                      data-bs-toggle="pill"
                      data-bs-target="#pills-02"
                      type="button"
                      role="tab"
                      aria-controls="pills-02"
                      aria-selected="false"
                    >
                      Exercise Files
                    </button>
                  </li>
                  <li className="nav-item">
                    <button
                      className="nav-link"
                      id="pill-3"
                      data-bs-toggle="pill"
                      data-bs-target="#pills-03"
                      type="button"
                      role="tab"
                      aria-controls="pills-03"
                      aria-selected="false"
                    >
                      Reviews
                    </button>
                  </li>
                </ul>
                <div className="tab-content" id="pills-tabContent">
                  <div
                    className="tab-pane fade show active"
                    id="pills-01"
                    role="tabpanel"
                    aria-labelledby="pill-1"
                  >
                    <div className="overview-area">
                      <h5>Course details</h5>
                      <p>
                        New to web design? Start here first. Instructor James
                        Williamson introduces the fundamental concepts, tools, and
                        learning paths for web design. He explains what it means
                        to be a web designer, the various areas of specialization,
                        and whether web design is the right hobby or career for
                        you. Along the way, he talks to five prominent designers
                        and developers, who have each found success in a different
                        corner of the web.
                      </p>
                      <div className="bg-gray">
                        <h6>What Will I Learn?</h6>
                        <div className="row">
                          <div className="col-md-12">
                            <ul>
                         
                              <li>
                                <i className="fa fa-check"></i>Create a static
                                homepage useful for most websites, or a blog like
                                homepage useful for bloggers.
                              </li>
                              <li>
                                <i className="fa fa-check"></i>Create an affiliate
                                site for passive, recurring income
                              </li>
                              <li>
                                <i className="fa fa-check"></i>Create a Responsive
                                Website that looks good on any browser.
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                      <h6>Requirements</h6>
                      <ul>
                        <li>
                          <i className="fa fa-check"></i>No previous experience or
                          software needed!
                        </li>
                        <li>
                          <i className="fa fa-check"></i>An open mind!
                        </li>
                      </ul>
                      <h6 className="mt-5">Skills covered in this course</h6>
                      <ul>
                        <li>
                          <i className="fa fa-check"></i>This course is great for
                          beginners who are still learning the financial markets.
                        </li>
                        <li>
                          <i className="fa fa-check"></i>This course is perfect
                          for you if you are taking over an existing Wordpress
                          website, or want to build one from scratch, but don't
                          know where to start.
                        </li>
                        <li>
                          <i className="fa fa-check"></i>If you want to learn to
                          master Wordpress without getting bogged down with
                          technical jargon, this course is for you.
                        </li>
                      </ul>
                      <div className="reviewers-area">
                        <div className="row">
                          <div className="col-lg-5">
                            <div className="media d-flex align-items-center">
                              <div className="thumb">
                                <img src="/assets/img/author/01.png" alt="img" />
                              </div>
                              <div className="media-body">
                                <h6><Link to={`/teacher-detail/${teacherData.id}`}>{teacherData.full_name}</Link></h6>
                                <span>Product Designer</span>
                              </div>
                            </div>
                          </div>
                          <div className="col-lg-7">
                            <p className="review-content">
                              Great for the people that are willing to improve and
                              learn. Please show up to the course with an open
                              mind because the instructor got his own views and
                              philosophy towards design that might challenge your
                              own. This course will teach you...
                            </p>
                          </div>
                        </div>
                        <div className="meta-area d-flex">
                          <div className="user-rating ms-0">
                            <span>
                              <i className="fa fa-star"></i>
                              <i className="fa fa-star"></i>
                              <i className="fa fa-star"></i>
                              <i className="fa fa-star"></i>
                              <i className="fa fa-star-half-alt"></i>
                              4.9
                            </span>
                            (76)
                          </div>
                          <div className="ms-auto">
                            <i
                              className="fa fa-user me-2"
                              style={{ color: "var(--main-color)" }}
                            ></i>
                            6794 students
                          </div>
                          <div className="ms-md-5 ms-auto mb-0">
                            <i
                              className="far fa-user me-2"
                              style={{ color: "var(--main-color)" }}
                            ></i>
                            6794 students
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div
                    className="tab-pane fade"
                    id="pills-02"
                    role="tabpanel"
                    aria-labelledby="pill-2"
                  >
                    <div className="col-lg-12 sidebar-area">
                      <div className="widget widget-accordion-inner">
                        <h5 className="widget-title border-0">Quiz</h5>
                        <div className="accordion" id="accordionExample">
                        <CourseQuizList />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div
                    className="tab-pane fade"
                    id="pills-03"
                    role="tabpanel"
                    aria-labelledby="pill-3"
                  >
                    <h5>Rating & Review</h5>
                    <div className="row">
                      <div className="col-sm-8">
                        <div className="single-input-wrap">
                          <textarea
                            rows="2"
                            placeholder="Enter Your Project Details...."
                            onChange={handleChange}
                            name="reviews"
                          ></textarea>
                        </div>
                      </div>
                      <div className="col-sm-4">
                        <div className="row mx-auto">
                          <div className="col-12">
                            <div
                              className="rating"
                              style={{ textalign: "center" }}
                            >
                              <select onChange={handleChange} className='form-control' name='rating'>
                              <option value="1">★</option>
                              <option value="2">★★</option>
                              <option value="3">★★★</option>
                              <option value="4">★★★★</option>
                              <option value="5">★★★★★</option>
                              
                              </select>
                            </div>
                          </div>
                          <div className="col-12">
                            <button
                              className="btn btn-base ms-auto "
                              type="submit" onClick={formSubmit}
                            >
                              Submit
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <hr></hr>
                    <div className="row">
                      <div className="reviewers-area">
                        <div className="row">
                          <div className="col-lg-3">
                            <div className="media align-items-center">
                              <div className="thumb">
                                <img src="/assets/img/author/01.png" alt="img" />
                              </div>
                              <div className="media-body">
                                <h6>Jessica Jessy</h6>
                                <span>Product Designer</span>
                              </div>
                            </div>
                          </div>
                          <div className="col-lg-9">
                            <div className="user-rating ms-0">
                              <span>
                                <svg
                                  className="svg-inline--fa fa-star fa-w-18"
                                  ariahidden="true"
                                  focusable="false"
                                  dataprefix="fa"
                                  dataicon="star"
                                  role="img"
                                  xmlns="http://www.w3.org/2000/svg"
                                  viewBox="0 0 576 512"
                                  data-fa-i2svg=""
                                >
                                  <path
                                    fill="currentColor"
                                    d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"
                                  ></path>
                                </svg>
                                <svg
                                  className="svg-inline--fa fa-star fa-w-18"
                                  ariahidden="true"
                                  focusable="false"
                                  dataprefix="fa"
                                  dataicon="star"
                                  role="img"
                                  xmlns="http://www.w3.org/2000/svg"
                                  viewBox="0 0 576 512"
                                  data-fa-i2svg=""
                                >
                                  <path
                                    fill="currentColor"
                                    d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"
                                  ></path>
                                </svg>
                                <svg
                                  className="svg-inline--fa fa-star fa-w-18"
                                  ariahidden="true"
                                  focusable="false"
                                  dataprefix="fa"
                                  dataicon="star"
                                  role="img"
                                  xmlns="http://www.w3.org/2000/svg"
                                  viewBox="0 0 576 512"
                                  data-fa-i2svg=""
                                >
                                  <path
                                    fill="currentColor"
                                    d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"
                                  ></path>
                                </svg>
                                <svg
                                  className="svg-inline--fa fa-star fa-w-18"
                                  ariahidden="true"
                                  focusable="false"
                                  dataprefix="fa"
                                  dataicon="star"
                                  role="img"
                                  xmlns="http://www.w3.org/2000/svg"
                                  viewBox="0 0 576 512"
                                  data-fa-i2svg=""
                                >
                                  <path
                                    fill="currentColor"
                                    d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"
                                  ></path>
                                </svg>
                                <svg
                                  className="svg-inline--fa fa-star-half-alt fa-w-17"
                                  ariahidden="true"
                                  focusable="false"
                                  dataprefix="fa"
                                  dataicon="star-half-alt"
                                  role="img"
                                  xmlns="http://www.w3.org/2000/svg"
                                  viewBox="0 0 536 512"
                                  data-fa-i2svg=""
                                >
                                  <path
                                    fill="currentColor"
                                    d="M508.55 171.51L362.18 150.2 296.77 17.81C290.89 5.98 279.42 0 267.95 0c-11.4 0-22.79 5.9-28.69 17.81l-65.43 132.38-146.38 21.29c-26.25 3.8-36.77 36.09-17.74 54.59l105.89 103-25.06 145.48C86.98 495.33 103.57 512 122.15 512c4.93 0 10-1.17 14.87-3.75l130.95-68.68 130.94 68.7c4.86 2.55 9.92 3.71 14.83 3.71 18.6 0 35.22-16.61 31.66-37.4l-25.03-145.49 105.91-102.98c19.04-18.5 8.52-50.8-17.73-54.6zm-121.74 123.2l-18.12 17.62 4.28 24.88 19.52 113.45-102.13-53.59-22.38-11.74.03-317.19 51.03 103.29 11.18 22.63 25.01 3.64 114.23 16.63-82.65 80.38z"
                                  ></path>
                                </svg>
                                4.9
                              </span>
                            </div>
                            <p className="review-content">
                              Great for the people that are willing to improve and
                              learn. Please show up to the course with an open
                              mind because the instructor got his own views and
                              philosophy towards design that might challenge your
                              own. This course will teach you...
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
                  
              <div className="col-lg-4 sidebar-area" >
                <div className="widget widget-course-details mb-0">
                  <h5 className="widget-title">Course Details</h5>
                  <ul>
                    <li>
                      Level: <span>Beginner</span>
                    </li>
                    <li>
                      Technology:{}
                      <span>
                      {courseData.techs}
                      </span>
                    </li>
                    <li>
                      Total Hour: <span>07h 30m</span>
                    </li>
                    <li>
                      Total Views: <span> {courseViews}</span>
                    </li>
                    <li>
                      Total Enrolled: <span>{courseData.total_enrolled_students}</span>
                    </li>
                    <li>
                      Last Update: <span>{courseData.time}</span>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          
       
          </div>
        </section>
        {simulatuionData ? simulatuionData.slice(0,1).map((item) => (
                            
                            <>
                            
                            <iframe src={item.index_file} style={{width:"100%",height:"100%"}}></iframe>

                          <h3 class="main-vid-title">{item.title}</h3>
                          </>  

                        
                      )) : <p>no post yet!</p>}
      </>
  )}

export default CourseDetail
